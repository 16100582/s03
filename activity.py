# add validation for leap year input
# strings are not allowed for inputs
# no zero or negative values
quit = False
while(quit != True):
    print('Part 1')
    year = input('Enter a year. \nType exit to move on to the next part. \n')
    if(year == 'exit'):
        quit = True
        break
    if(any(c.isalpha() for c in year)):
        print('Please enter numbers only.')
        continue
    elif(int(year) <= 0):
        print('Please enter a positive number.')
        continue
    else:
        year = int(year)
    if (year%4==0 and year%100!=0) or year%400==0:
        print(f'{year} is a leap year')
    else:
        print(f'{year} is not a leap year')
quit=False
while(quit != True):
    w,h=0,0
    w=input('Enter a width. \nType exit to quit. \n')
    if(w == 'exit'):
        quit = True
        break
    h=input('Enter a height. \nType exit to quit. \n')
    if(h == 'exit'):
        quit = True
        break
    if(any(c.isalpha() for c in w) or any(c.isalpha() for c in h)):
        print('Please enter numbers only.')
        continue
    else:
        w,h=int(w),int(h)
    for i in range(h):
        print('*' * w + ' ')